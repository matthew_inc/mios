import os
import colorama as cr
os.system("clear")
cr.init(autoreset=True)
print("miOS Kern v2.1 Alpha\n© 2020 Matthew_inc.")
usr_load = open("Users/User", "r")
usr_read = usr_load.read()

# ------------------- Login ------------------- #

usr_try = input("Username: ")
while usr_try != usr_read:
    os.system("clear")
    print(f"{cr.Fore.RED}Incorrect.")
    usr_try = input("Username: ")
if usr_try == usr_read:
    os.system("clear")
    print(f"{cr.Fore.GREEN}Correct.")
    pass_load = open("Users/Password", "r")
    pass_read = pass_load.read()
    pass_try = input("Password: ")
    while pass_try != pass_read:
        os.system("clear")
        print(f"{cr.Fore.RED}Incorrect.")
        pass_try = input("Password: ")
    os.system("clear")
    print(f"{cr.Fore.GREEN}Correct.{cr.Fore.YELLOW}\nUse the command \"help\" to see more info.")
    # -----Main Loop----- #
    while True:
        command = input(f"{cr.Fore.GREEN}>> ")
        if command.strip() == "":
            pass
        if command == "help":
            print(f"{cr.Fore.YELLOW}You are using miOS.\nList of Commands:\nexit\nwhoami\ncredits")
        elif command == "exit":
            exit()
        elif command == "whoami":
            print(usr_read)
        elif command == "credits":
            print(f"{cr.Back.WHITE}{cr.Fore.BLACK}Created by Matthew_inc.\n©2019 Matthew_inc.")
        else:
            os.system("python Apps/" + command + ".py")
