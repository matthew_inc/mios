import colorama as cr
import os
cr.init(autoreset=True)


def delete():
    print(f"{cr.Fore.RED}WARNING: You are about to delete a file.")
    ask_delete = input("Are You Sure? (Y/N): ")
    if ask_delete == "Y":
        txt = input("File to delete: ")
        if os.PathExists:
            os.system("rm Documents/" + txt)
    elif ask_delete == "N":
        print("Canceling...")


def edit():
    file_ask_write = input("File name to edit/create: ")
    file_load_write = open("%s" % file_ask_write, "a")
    file_load_read = open("%s" % file_ask_write, "r")
    print(f"{cr.Fore.GREEN}Loaded!")
    file_read = file_load_read.read()
    file_load_read.close()
    file_ask_write = input(file_read)
    file_load_write.write(f"{cr.Fore.YELLOW}\n" + file_ask_write)
    file_load_write.close()


cr.init(autoreset=True)
choice = input("delete, edit, view, list\nWhat Would You Like To Do: ")
if choice == "delete":
    delete()
elif choice == "edit":
    edit()
